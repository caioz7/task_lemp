# task_lemp

- Essa task é a seguinte:
  - [X] Registre um domínio gratuíto, no meu caso dbdantas.tk
  - [X] Configure o DNS do domínio no Route 53 definindo o registro A para o IP da instancia EC2
  - [X] Configure uma instancia RDS com MySQL
  - [X] Configure uma instancia EC2 com CentOS 7
  - [X] Instale e configure o NGINX
  - [X] Instale e configure o PHP 7.0
  - [X] Instale e configure o Wordpress
  - [X] Instale e configure o Magento
  - [X] Instale e configure o Tomcat com proxy reverso
  - [X] SSL(certbot)

Atualizar e instalar os seguintes pacotes:
```bash
yum update -y && yum -y install gcc make wget vim net-tools tmux wget zip unzip epel-release yum-utils
```

```bash
# Acessando a instancia
ssh -i "CHAVE.pem" centos@IP
```
- DESABILITANDO SElinux
```bash
sudo vim /etc/sysconfig/selinux
```

Alterar para **disabled**, like this:
```bash
SELINUX=disabled
```

- Instalando PHP 7.0
```bash
sudo yum update -y
sudo yum -y install epel-release
sudo yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
sudo yum install yum-utils -y
sudo yum-config-manager --enable remi-php70
sudo yum update && sudo yum install -y php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo \
php-fpm php-json php-opcache php-mbstring php-xml
# confirmar se foi instalada a versão 7.0
php --version
```

- Instalando nginx
```bash
yum -y install nginx
```

- Verificando serviço do nginx, iniciando e deixando seu inicio automatico
```bash
systemctl status nginx
systemctl start nginx
chkconfig nginx on
```

- Iniciando o serviço e ativando o **PHP-FPM**
```bash
systemctl start php-fpm
systemctl status php-fpm
chkconfig php-fpm on
```

- Para configurar o **PHP-FPM** para o **NGINX** descomente as linhas e altere o usuario e grupo de permissão para o nginx
```bash
vim /etc/php-fpm.d/www.conf

;listen = 127.0.0.1:9000  --> listen = /var/run/php-fpm/php-fpm.sock  
;listen.owner = nobody --> listen.owner = nginx  
;listen.group = nobody --> listen.group = nginx  
;listen.mode = 0666  --> listen.mode = 0664  
user = nginx  
group = nginx  
```

- Reiniciando o php-fpm para aplicar as alterações
```bash
systemctl restart php-fpm
# Acesse o endereço IP pelo navegador para testar, lembre-se de ajustar o Security Group!
```
- Crie um instancia **RDS(MySQL)**
https://sa-east-1.console.aws.amazon.com/rds/home?

**Documentação**: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Tutorials.WebServerDB.CreateDBInstance.html


- instalando o MySQL e testando a conexão(lembre-se de ajustar o SG do RDS para acesso)
```bash
yum install mysql
mysql -u admin -h HOST -p

# Importando aquivo sql para criar usuários e bancos(loja,blog)
mysql -u admin -h HOST -p < create_databases.sql
```
- O arquivo .sql com os comandos pode ser copiado através do scp:
```bash
scp -i ~/keys/CHAVE.pem create_databases.sql centos@IP:~/
```

- Preparando e instalando **Wordpress**

 ```bash
mkdir -p /var/www/html/blog-dbdantas #Criando diretorio para o site
vim /etc/nginx/conf.d/blog_dbdantas.conf #Criando conf do virtual host, inserir o conteudo do arquivo da pasta virtual_hosts
nginnginx -t # Testando a sintaxe do virtual host, se estiver ok, prossiga!

- Baixando a ultima versão, extraindo, movendo os arquivos para o diretório e dando as permissões do nginx
wget https://wordpress.org/latest.tar.gz
tar -zxvf latest.tar.gz        
mv wordpress/* /var/www/html/blog-dbdantas
chown -R nginx: /var/www/html/blog-dbdantas/


Então acesse o site e insira os dados, nome do site, url, dados do banco, etc.
```

- Instalando o Magento sobre o domínio **loja.dbdantas.tk**
```bash
mkdir -p /var/www/html/loja-dbdantas
# Instalando o composer para o PHP na versão 1 devido a compatibilidade
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer --version=1.10.16

# Baixando o Magento, extraindo e movendo os arquivos para o diretório:
wget https://github.com/magento/magento2/archive/2.1.zip
unzip 2.1.zip
mv magento2-2.1/* /var/www/html/loja-dbdantas/
chown -R nginx: /var/www/html/loja-dbdantas/

# Instalando o magento utilizando o composer:
cd /var/www/html/loja-dbdantas/
composer install -v
```

- Agora acesse o site pelo navegador e siga com as instruções, coloque os dados correspondentes ao seu Banco criado,
URL com HTTPS, usuário para acessar o console admin, moeda, etc


- Após a instalação caso receba o **Erro 500** na pagina, configure as permissões conforme abaixo:
```bash
cd /var/www/html/loja-dbdantas/
find . -type f -exec chmod 644 {} \; # 644 permissão para arquivos
find . -type d -exec chmod 755 {} \; # 755 permissão para pastas
chown -R nginx: *
chmod u+x bin/magento
```
- Instale o Java jdk para o uso do **TOMCAT**:
```bash
yum install java-11-openjdk-devel
# Lembre-se de configurar sua variavel $JAVA_HOME
```
![](images/bashrc_tomcat.png)
![](images/bashrc_tomcat_2.png)

- Criando grupo e usuário para o **TOMCAT**:
```bash
groupadd tomcat
useradd -s /bin/false -g tomcat -d /home/tomcat tomcat 
```
- Baixando, instalando e configurando o **TOMCAT**
```bash
# Download via wget e extraindo:
wget https://downloads.apache.org/tomcat/tomcat-9/v9.0.39/bin/apache-tomcat-9.0.39.tar.gz
tar -zxvf apache-tomcat-9.0.39-fulldocs.tar.gz

# Agora mova a pasta do tomcat para um diretorio, no meu caso no /opt/tomcat
mv tomcat-9.0-doc/ /opt/tomcat

# Bora mudar essas permissões!
chown -R tomcat: /opt/tomcat
chmod -R 755 /opt/tomcat/

# crie o arquivo de service com o conteu que temos aqui em services/tomcat.service
vim /etc/systemd/system/tomcat.service # Preste atenção nos diretórios!

# Agora de um reload no systemd:
systemctl daemon-reload

# Inicie o serviço e o ative para inicar automaticamente na inicialização do CentOS
systemctl start tomcat
systemctl enable tomcat
```
# Publicando o TOMCAT(tomcat.dbdantas.tk)
>   Agora com o Tomcat instalado e configurado precisamos publica-lo, mas não pela porta 8080, para isso precisamos realizar o proxy reverso
> para que ao acessarmos pela porta padrão do HTTP 80(443 para HTTPS) ele acesse na porta do tomcat 8080,
> então utilize o Virtual Host tomcat_dbdantas.conf que está no diretório.
>  Como pode ver no vhost do tomcat ele não aponta para um diretório como os anteriores, mas sim para uma porta em localhost: http://127.0.0.1:8080/
>  Essa é a jogada de mestre do proxy reverso!
>  Agora acesse seu link configurado no Route 53 para o tomcat, no meu caso é esse: http://tomcat.dbdantas.tk Se tudo ocorreu bem, você deve ver essa pagina:


![](images/site_tomcat.png)

- Agora bora deixar estes sites com SSL usando o certbot, siga os seguintes comandos:
```bash
# Instalando certbot
yum install httpd mod_ssl python-certbot-nginx

# Verificando versão
certbot --version

# Configurando Firewall e abrindo as portas 80(HTTP) e 443(HTTPS)

firewall-cmd --add-service=http
firewall-cmd --add-service=https
firewall-cmd --runtime-to-permanent

# Caso esteja rodando o iptables, use os comandos abaixo:
iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -I INPUT -p tcp -m tcp --dport 443 -j ACCEPT

# Instalando os certificados nos domínios dbdantas.tk, blog.dbdantas.tk e loja.dbdantas.tk

certbot --nginx -d dbdantas.tk -d www.dbdantas.tk
certbot --nginx -d blog.dbdantas.tk
certbot --nginx -d loja.dbdantas.tk
```
- Testar via: 
https://www.ssllabs.com/ssltest/analyze.html?d=DOMINIO.COM