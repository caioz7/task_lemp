create database blog_dbdantas;
create database loja_dbdantas;

CREATE USER 'user_loja'@'172.31.2.22' identified by 'lojaMND02';
CREATE USER 'user_blog'@'172.31.2.22' identified by 'blogMND02';

grant all on loja_dbdantas.* to 'user_loja'@'172.31.41.78';
grant all on blog_dbdantas.* to 'user_blog'@'172.31.41.78';
flush privileges;
